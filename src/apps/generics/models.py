# encoding: utf-8

from django.db import models


class CreatedMixin(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class UpdatedMixin(models.Model):
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TimestampedModel(CreatedMixin, UpdatedMixin):
    class Meta:
        abstract = True
