from logging import basicConfig, getLogger, DEBUG, INFO, WARNING, ERROR

from django.core.management.base import BaseCommand

from core.broker import DumbBroker


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('identifier', nargs='+')
        parser.add_argument('--provider', nargs='?')

    def handle(self, *args, **options):
        verbosity = int(options['verbosity'])
        basicConfig(level={
            0: ERROR,
            1: WARNING,
            2: INFO,
            3: DEBUG,
        }.get(verbosity, DEBUG))

        provider = options['provider']
        for identifier in options['identifier']:
            broker = DumbBroker(identifier=identifier, provider=provider)
            broker.sync()
