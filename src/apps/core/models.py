# encoding: utf-8

from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from generics.models import TimestampedModel


STATUS_UNKNOWN = 'unknown'
STATUS_INVALID = 'invalid'
STATUS_PENDING = 'pending'
STATUS_IN_PROGRESS = 'in progress'
STATUS_FAILED = 'failed'
STATUS_UPDATED = 'updated'

STATUSES = (
    (STATUS_UNKNOWN, _('unknown'), ),
    (STATUS_INVALID, _('invalid'), ),
    (STATUS_PENDING, _('pending'), ),
    (STATUS_IN_PROGRESS, _('in progress'), ),
    (STATUS_FAILED, _('failed'), ),
    (STATUS_UPDATED, _('updated'), ),
)


class Tracking(TimestampedModel, models.Model):
    provider = models.CharField(max_length=32, db_index=True, null=True)
    identifier = models.CharField(max_length=128, db_index=True)

    source_country = models.CharField(max_length=256, blank=True)
    destination_country = models.CharField(max_length=256, blank=True)

    class Meta:
        unique_together = (
            ('provider', 'identifier', ),
        )

    def __repr__(self):
        return '{} ({})'.format(self.identifier,
                                self.provider or 'unknown provider')
    __unicode__ = __repr__
    __str__ = __repr__

    @cached_property
    def last_sync(self):
        return self.syncs.all().first()


class TrackingSync(TimestampedModel, models.Model):
    tracking = models.ForeignKey(Tracking, related_name='syncs')
    status = models.CharField(max_length=24, default=STATUS_PENDING)
    task_id = models.UUIDField(null=True)

    class Meta:
        ordering = '-id',


class TrackingCheckpoint(TimestampedModel, models.Model):
    tracking = models.ForeignKey(Tracking, related_name='checkpoints')

    message = models.CharField(max_length=256, blank=True, null=True)
    location = models.CharField(max_length=256, blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = '-id',
