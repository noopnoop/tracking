# encoding: utf-8
from functools import lru_cache

from django.conf import settings
from redis import StrictRedis


CORE_REDIS_HOST = getattr(settings, 'CORE_REDIS_HOST', 'redis')
CORE_REDIS_PORT = getattr(settings, 'CORE_REDIS_PORT', 6379)
CORE_REDIS_DB = getattr(settings, 'CORE_REDIS_DB', 0)
CORE_REDIS_PASSWORD = getattr(settings, 'CORE_REDIS_PASSWORD', '')


@lru_cache()
def get_redis_connection():
    return StrictRedis(host=CORE_REDIS_HOST, port=CORE_REDIS_PORT,
                       db=CORE_REDIS_DB, password=CORE_REDIS_PASSWORD)
