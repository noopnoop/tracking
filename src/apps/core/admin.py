# encoding: utf-8

from django.contrib.admin import ModelAdmin, register, TabularInline

from core import models


class TrackingCheckPointInline(TabularInline):
    model = models.TrackingCheckpoint
    extra = 0


@register(models.Tracking)
class TrackingModelAdmin(ModelAdmin):
    list_display = \
        'provider', \
        'identifier', \
        'created', \
        'updated',
    inlines = TrackingCheckPointInline,


@register(models.TrackingSync)
class TrackingSyncModelAdmin(ModelAdmin):
    list_display = \
        'tracking', \
        'status', \
        'task_id', \
        'created', \
        'updated',

    def get_queryset(self, request):
        return super(TrackingSyncModelAdmin, self).\
            get_queryset(request).\
            prefetch_related('tracking')


@register(models.TrackingCheckpoint)
class TrackingCheckPointModelAdmin(ModelAdmin):
    list_display = \
        'tracking', \
        'message', \
        'location', \
        'timestamp', \
        'created',

    def get_queryset(self, request):
        return super(TrackingCheckPointModelAdmin, self).\
            get_queryset(request). \
            prefetch_related('tracking')
