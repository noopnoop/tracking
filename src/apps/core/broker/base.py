# encoding: utf-8

from logging import getLogger


logger = getLogger(__name__)


class BaseBroker:
    def __init__(self, identifier, provider=None):
        self._identifier = identifier
        self._provider = provider
        self._instance = None

    @property
    def provider(self):
        if self._instance:
            return self._instance.provider
        return self._provider

    @property
    def identifier(self):
        if self._instance:
            return self._instance.identifier
        return self._identifier

    @property
    def instance(self):
        if self._instance is None:
            self._instance = self.get_instance(self.identifier, self.provider)
        return self._instance

    def retrieve(self):
        raise NotImplementedError

    def get_instance(self, identifier, provider=None):
        raise NotImplementedError

    def update_instance(self, **kwargs):
        raise NotImplementedError

    def create_sync_status(self, task_id):
        raise NotImplementedError

    def update_sync_status(self, status, **kwargs):
        raise NotImplementedError

    def update_results(self, checkpoints, **kwargs):
        raise NotImplementedError
