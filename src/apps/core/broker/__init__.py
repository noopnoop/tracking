# encoding: utf-8

from . import mixins
from . import base
from . import stores


class DBBroker(stores.DBStoreMixin, mixins.ProvidersMixin, base.BaseBroker):
    def retrieve(self):
        if self.is_expired:
            self.invalidate()
        return self.instance

Broker = DBBroker


class DumbBroker(stores.DumbStore, mixins.ProvidersMixin, base.BaseBroker):
    def retrieve(self):
        pass
