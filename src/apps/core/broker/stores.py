# encoding: utf-8

from pprint import pformat
from logging import getLogger

from django.db.transaction import atomic

from core import models
from .mixins import BaseBroker


logger = getLogger(__name__)


class DumbStore(BaseBroker):
    def retrieve(self):
        pass

    def get_instance(self, identifier, provider=None):
        logger.info('Get instance\n%s', pformat({
            'identifier': identifier,
            'provider': provider,
        }))

    def update_instance(self, **kwargs):
        logger.info('Update instance\n%s', pformat({
            'kw': kwargs,
        }))

    def create_sync_status(self, task_id):
        logger.info('Create sync status:\n%s', pformat({
            'task_id': task_id,
        }))

    def update_sync_status(self, status, **kwargs):
        logger.info('Update sync status:\n%s', pformat({
            'kw': kwargs,
            'status': status,
        }))

    def update_results(self, checkpoints, **kwargs):
        logger.info('Update results:\n%s', pformat({
            'kw': kwargs,
            'checkpoints': checkpoints,
        }))


class DBStoreMixin(BaseBroker):
    def __init__(self, *args, **kwargs):
        super(DBStoreMixin, self).__init__(*args, **kwargs)
        self._last_sync_id = None

    def get_instance(self, identifier, provider=None):
        qs = models.Tracking.objects. \
            filter(identifier=identifier)
        if provider:
            qs = qs.filter(provider=provider)
        if qs.exists():
            return qs.first()

        instance = models.Tracking.objects. \
            create(identifier=identifier,
                   provider=provider)
        return instance

    def update_instance(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self.instance, key, value)
        models.Tracking.objects. \
            filter(id=self.instance.id). \
            update(**kwargs)

    def create_sync_status(self, **kwargs):
        return models.TrackingSync.objects. \
            create(tracking=self.instance, **kwargs)

    def _get_last_sync_id(self):
        self._last_sync_id = models.TrackingSync.objects. \
            filter(tracking_id=self.instance.id). \
            values_list('id', flat=True)
        if not self._last_sync_id:
            return self.create_sync_status().id

        return self._last_sync_id[0]

    def update_sync_status(self, status, **kwargs):
        if self._last_sync_id is None:
            self._last_sync_id = self._get_last_sync_id()

        models.TrackingSync.objects. \
            filter(id=self._last_sync_id). \
            update(status=status, **kwargs)

    @atomic
    def update_results(self, checkpoints, **kwargs):
        kwargs = {k: v for k, v in kwargs.items() if v}

        if kwargs:
            self.update_instance(**kwargs)

        if checkpoints:
            items = [
                models.TrackingCheckpoint(tracking=self.instance, **item)
                for item in checkpoints
            ]
            models.TrackingCheckpoint.objects.\
                filter(tracking=self.instance.id).\
                delete()
            models.TrackingCheckpoint.objects. \
                bulk_create(items)
