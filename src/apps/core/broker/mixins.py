# encoding: utf-8

from logging import getLogger

from core import models
from core.conf import get_redis_connection
from providers import get_providers, get_provider
from providers import errors
from .base import BaseBroker


logger = getLogger(__name__)


class ProvidersMixin(BaseBroker):
    TTL = 60 * 5

    def invalidate(self):
        from core.tasks import sync
        get_redis_connection().setex(self.key, self.TTL, None)
        async_result = sync.apply_async(kwargs={
            'identifier': self.identifier,
            'provider': self.provider,
        })
        self.create_sync_status(task_id=async_result.task_id)

    @property
    def key(self):
        return '{}-{}'.format(self.identifier, self.provider)

    @property
    def is_expired(self):
        return not get_redis_connection().exists(self.key)

    @property
    def provider_classes(self):
        # провайдер известен - возврат по алиасу
        if self.provider:
            yield get_provider(self.provider)
            raise StopIteration

        # провайдер неизвестен - попытка сматчить
        for alias, provider in get_providers():
            if not provider.match(self.identifier):
                continue
            yield provider
            raise StopIteration

        # перебор всех
        for _, provider in get_providers():
            yield provider

    def sync(self):
        provider_classes = list(self.provider_classes)
        known_provider = bool(self.provider)
        status = None
        some_was_failed = False

        logger.info('Sync (id=%s provider=%s): providers %s',
                    self.identifier, self.provider,
                    [provider.alias for provider in provider_classes])

        # перебор провайдеров (если известен заранее, то будет только один)
        for provider_class in provider_classes:
            try:
                provider = provider_class(broker=self)
                provider.fetch()

                if self.instance and not self.instance.provider:
                    # провайдер стал известен и можно сохранить
                    self.update_instance(provider=provider_class.alias)

                self.update_sync_status(models.STATUS_UPDATED)
                return True

            except errors.ProviderUnavailable:
                if known_provider:
                    break
                some_was_failed = True
                continue

            except (errors.InvalidIdentifier, errors.InvalidProvider):
                if known_provider:
                    status = models.STATUS_INVALID
                    break
                continue

        # подгон итогового статуса
        if not status:
            if known_provider:
                status = models.STATUS_FAILED
            else:
                if not some_was_failed:
                    # у какого-то из провайдеров была ошибка,
                    # но можно попробовать снова
                    status = models.STATUS_FAILED
                else:
                    # идентификатор был невалидным для всех
                    status = models.STATUS_INVALID

        logger.debug('Sync (id=%s provider=%s) result {}', self.identifier,
                     self.provider, status)
        self.update_sync_status(status)
