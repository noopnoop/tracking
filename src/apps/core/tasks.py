# encoding: utf-8

from celery import task

from core.broker import Broker


@task(name='core.sync')
def sync(identifier, provider=None):
    broker = Broker(identifier, provider)
    broker.sync()
