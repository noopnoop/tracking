# encoding: utf-8

from itertools import chain
from datetime import datetime
from pytz import utc

import requests
import pycountry

from .base import Provider
from . import errors


SRC, TRANSFER, DST = range(3)


class SypostNetProvider(Provider):
    alias = 'sytrack'

    def fetch(self):
        response = requests.post('http://www.sypost.net/query', data={
            'connotNo': self.broker.identifier,
        })
        if response.status_code != 200:
            raise errors.ProviderUnavailable

        content = response.json()
        if 'data' not in content or len(content['data']) <= 0:
            raise errors.ProviderUnavailable

        item = content['data'][0]
        if not item['has']:
            raise errors.InvalidIdentifier

        destination_country = self.country_by_code(item['dstCountry'])
        source_country = self.country_by_code(item['orgCountry'])

        origin = self.get_key(item, 'result', 'origin', 'items') or []
        origin = [
            self.prepare_checkpoint(point, SRC)
            for point in origin
        ]

        transfer = self.get_key(item, 'result', 'transfer', 'items') or []
        transfer = [
            self.prepare_checkpoint(point, TRANSFER)
            for point in transfer
        ]

        destination = self.get_key(item, 'result', 'transfer', 'items') or []
        destination = [
            self.prepare_checkpoint(point, DST)
            for point in destination
        ]

        timeline = self.timeline(origin, transfer, destination)
        self.broker.update_results(list(timeline),
                                   source_country=source_country,
                                   destination_country=destination_country)

    def country_by_code(self, code):
        if not code:
            return
        try:
            return pycountry.countries.get(alpha_2=code).name
        except:
            pass

    def prepare_checkpoint(self, checkpoint, type):
        timestamp = checkpoint.get('createTime', None)
        if timestamp:
            timestamp = datetime.fromtimestamp(timestamp / 1000, utc)

        office = checkpoint.get('office', None)

        return {
            'message': checkpoint.get('content', None),
            'location': office,
            'timestamp': timestamp,
            # 'type': type,
        }

    def get_key(self, item, *keys):
        value = item.copy()
        for key in keys:
            value = (value or {}).get(key)
        return value

    def timeline(self, origin, transfer, destination):
        # TODO:
        return chain(origin, transfer, destination)
