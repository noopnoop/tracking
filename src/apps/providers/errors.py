# encoding: utf-8


class ProviderError(RuntimeError):
    pass


class UnknownProvider(ProviderError):
    pass


class InvalidProvider(ProviderError):
    pass


class InvalidIdentifier(ProviderError):
    pass


class ProviderUnavailable(ProviderError):
    pass
