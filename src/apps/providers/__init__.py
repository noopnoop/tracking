from .base import ProviderMeta
from .errors import UnknownProvider
from . import sypost_net


def get_providers():
    return ProviderMeta.register.items()


def get_provider_aliases():
    return ProviderMeta.register.keys()


def get_provider(alias):
    try:
        return ProviderMeta.register[alias]
    except KeyError:
        raise UnknownProvider
