# encoding: utf-8

from abc import ABCMeta, abstractmethod
from weakref import ref


class ProviderMeta(ABCMeta):
    register = {}

    def __new__(mcls, name, bases, namespace):
        cls = super(ProviderMeta, mcls).__new__(mcls, name, bases, namespace)
        abstract = namespace.pop('abstract', False)

        if not abstract:
            alias = namespace.get('alias')
            assert alias
            assert alias not in ProviderMeta.register
            ProviderMeta.register[alias] = cls

        return cls


class Provider(metaclass=ProviderMeta):
    abstract = True
    alias = None

    def __init__(self, broker):
        self._broker = ref(broker)

    @property
    def broker(self):
        return self._broker()

    @classmethod
    def match(self, identifier):
        pass

    @abstractmethod
    def fetch(self):
        pass
