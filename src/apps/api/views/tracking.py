# encoding: utf-8

from rest_framework import status
from rest_framework.response import Response
from rest_framework import serializers
from rest_framework import views

from core import models
from core.broker import Broker
from providers import get_provider_aliases
from .base import APIMixin


PROVIDER_CHOICES = [
    (alias, alias)
    for alias in get_provider_aliases()
]


class RequestSerializer(serializers.Serializer):
    trackingNumber = serializers.CharField(max_length=128)
    source = serializers.ChoiceField(required=False, choices=PROVIDER_CHOICES)


class CheckpointSerializer(serializers.ModelSerializer):
    time = serializers.DateTimeField(source='timestamp')

    class Meta:
        model = models.TrackingCheckpoint
        fields = \
            'time', \
            'message', \
            'location',


class GeneralSerializer(serializers.ModelSerializer):
    sourceCountry = serializers.CharField(source='source_country')
    destinationCountry = serializers.CharField(source='destination_country')

    class Meta:
        model = models.Tracking
        fields = \
            'sourceCountry', \
            'destinationCountry',


class TrackingSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='last_sync.status')
    general = GeneralSerializer(source='*')
    checkpoints = CheckpointSerializer(many=True)

    class Meta:
        model = models.Tracking
        fields = \
            'status', \
            'general', \
            'checkpoints', \
            'identifier', \
            'provider',


class TrackingView(APIMixin, views.APIView):
    def get(self, request, *args, **kwargs):
        request_data = request.data or request.query_params
        request_serializer = RequestSerializer(data=request_data)
        if not request_serializer.is_valid():
            return Response(request_serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        data = request_serializer.data

        broker = Broker(identifier=data['trackingNumber'],
                        provider=data.get('source'))
        instance = broker.retrieve()

        serializer = TrackingSerializer(instance=instance)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)
