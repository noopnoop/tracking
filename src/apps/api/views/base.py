# encoding: utf-8

from rest_framework import authentication
from rest_framework import permissions
from rest_framework import renderers


class APIMixin:
    authentication_classes = authentication.SessionAuthentication,
    permission_classes = permissions.AllowAny,
    renderer_classes = renderers.JSONRenderer,
