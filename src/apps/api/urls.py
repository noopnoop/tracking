# encoding: utf-8

from django.conf.urls import url, include

from . import views


urlpatterns = [
    url(r'^', include([
        url(r'^tracking/', views.TrackingView.as_view(), name='tracking'),
    ], namespace='api')),
]
