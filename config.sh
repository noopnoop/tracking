#!/usr/bin/env bash

export PREFIX=tracker

export USER="$(id -u):$(id -g)"
export NETWORK_SUBNET=172.16.243.0/24

export PRETTY_SHELL=

export COMPOSE_HTTP_TIMEOUT=3600

export DOCKER_GATEWAY=$(ip ro | grep docker0 | awk '{print $NF}')
export DEBUGGER_HOST=$DOCKER_GATEWAY

function get-ip () {
  echo "$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $1)"
}

[[ -f ./config-overrides.sh ]] && . ./config-overrides.sh
