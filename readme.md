### Запуск

``` 
$ ./docker/compose.sh build
$ ./docker/compose.sh up -d redis
$ ./docker/compose.sh up -d postgres
$ ./docker/compose.sh up -d server celery 
$ ./docker/manage.sh migrate
$ ./docker/manage.sh loaddata fixtures/users.json 
```

[админка](http://172.16.243.101:8000/admin/) (пользователь: admin, пароль: admin)


### Остановка и удаление

``` 
$ ./docker/compose.sh stop
$ ./docker/compose.sh rm -f
$ docker volume prune
$ docker image prune
```
