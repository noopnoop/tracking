#!/usr/bin/env bash

. ./config.sh

FILES=(
  "-f ./docker/compose-generic.yml"
  "-f ./docker/compose-storages.yml"
  "-f ./docker/compose-services.yml"
)
if [[ ! -z "$PRETTY_SHELL" ]]; then
  FILES+=("-f ./docker/compose-pretty-shell.yml")
fi

docker-compose -p $PREFIX ${FILES[@]} $@
