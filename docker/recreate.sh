#!/usr/bin/env bash

SERVICE=${@:-server}
./docker/compose.sh build $SERVICE
./docker/compose.sh stop $SERVICE
./docker/compose.sh rm -f $SERVICE
./docker/compose.sh up -d $SERVICE
./docker/logs.sh $SERVICE
